(defsystem clos-conv
  :description "A small library allowing the user to convert CLOS classes to various datatypes and back."
  :version "0r1"
  :author "John Whitley <dogjaw2233@protonmail.com>"
  :license "Apache 2.0"
  :components ((:file "packages")
	       (:file "converter"
		      :depends-on "packages")))
